#!/usr/bin/python2.6
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 28 08:53:05 2015

This script will read in nwchem output files calculating gradients.
It will output the specified distance, the total DFT energy, and
the coulombic potential for the specified geometry.

Corrections, like adding X atoms for TIP4P or changing atom names (ie. O -> Om for 
calculations involving methanol) should be done before this program is run.

Make sure to specify variables in section 1.

@author: m.kelley
"""
from __future__ import print_function
from __future__ import division
import sys

"""
1. This section needs user specified variables
"""
#gradient_name = 'increasing.output.1572980.organized' # Name of gradient file? (ie. decreasing.output.1256226)
gradient_name = sys.argv[1]
atm_num = 28 # Number of atoms?
atoms_moved = [7, 28] # Which atoms are being scanned?

calc_type = 'MP2' # calculation recognizes either MP2 or DFT

# This is a dictionary containing atom names and their corresponding charges.
# Please edit the entires below, adding or removing as needed.
charge_dict = {} 
charge_dict['La'] = 3
charge_dict['Om'] = -0.7
charge_dict['Hm'] = 0.435
charge_dict['Hc'] = 0
charge_dict['C'] = 0.265
charge_dict['O'] = -0.834 #TIP4P 0
charge_dict['H'] = 0.417 #TIP4P 0.5564
charge_dict['M'] = -1.1128

"""
2. Functions used later in the program
"""
# Simple function for calculating distance between two specified sets of coordinates
def dist(x1, y1, z1, x2, y2, z2): 
    xdis = (float(x1) - float(x2)) ** 2
    ydis = (float(y1) - float(y2)) ** 2
    zdis = (float(z1) - float(z2)) ** 2
    sumdis = xdis + ydis + zdis
    dis = sumdis ** 0.5
    return dis
    
# Calculates distance between atoms specified in section 1 (atoms_moved). Uses x, y, and z 
# coordinate lists from the gradient section.
def distance(atoms, x, y, z):
    # Gets index number for atoms in lists
    atm1 = atoms[0] - 1
    atm2 = atoms[1] - 1
    
    # Finds coordinates for specified atoms
    x1 = x[atm1]
    y1 = y[atm1]
    z1 = z[atm1]
    x2 = x[atm2]
    y2 = y[atm2]
    z2 = z[atm2]
    
    # Calculates distance for specified atoms
    dis = dist(x1, y1, z1, x2, y2, z2)
    dis_ang = dis / 1.889725989 #converts distance to angstroms
    return dis_ang # Returns distance in angstroms

# Calculates the total coulombic energy for the geometry specified by each gradient.
# Input is the dictionary containing atoms names and charges, and the atom names and
# coordinates from the gradient section.
def charge(atm_num, charge_dict, name, x, y, z):
    total = 0 #keeps track of total coulombic energy

    # Setting up a list of charges in the same order as the atom list from the gradient file
    charge_list = []
    for i in range(atm_num):
        charge_list.append(charge_dict[name[i]]) # looks up charge for each atom
    
    # Will iterate through the atoms, calculating the coulombic energy between each atom
    # and every subsequent atom, updating the total energy
    for i in range(atm_num):
        m = i + 1
        for n in range(m,atm_num):
            dis = dist(x[i], y[i], z[i], x[n], y[n], z[n])
            coulE = charge_list[i] * charge_list[n] / dis
            total += coulE 
    
    # Converts total coulombic energy to kCal/mol
    # Actual conversion: *((1.602177E-19)^2)*89870000000000000000)*0.000239005736*(6.022E+23)
    energy = total * 332.0357166
    return energy # returns coulombic energy in kCal/mol
    
"""
3. This section sets up lists for energy, distance, and coulombic potential
"""
DFT_energy_list = [] # a list containing all Total DFT Energy values
distances = [] # a list containing each specified distance
coulomb_energy_list = [] # a list containing coulombic energy for each gradient calculation

grad_atom_list = [] # a temporary list containing each atom name used to extract data from each gradient
grad_x_list = [] # a temporary list containing each atom x coordinate in gradient
grad_y_list = [] # a temporary list containing each atom y coordinate in gradient
grad_z_list = [] # a temporary list containing each atom z coordinate in gradient

"""
4. This section searches the gradient file for relevant information
"""
grad_section = 0 # a variable that determines if a gradient section is currently being read
gradnum = 0 # counts number of gradients

gradient = open('%s' % (gradient_name), 'r') #opens gradient file specified in section 1
for line in gradient:
    data = line.strip().split() #splits gradient file into fields separated by a space
    try:
        if data[0] == "Total" and data[1] == calc_type and data[2] == "energy": #finds DFT energy
            dfte = float(data[4]) * 627.5095 # converts DFT energy from hartrees to kcal/mol
            DFT_energy_list.append(dfte) #adds found energy to list
        if data[1] == "ENERGY" and data[2] == "GRADIENTS": #finds gradient section of file
            grad_section = 1
        if grad_section == 1 and len(data) == 8:
            #checks for the actual atom lines in the gradient section
            #the length check will make sure a line has #, atom, xyz for coordinates, and xyz for gradients
            #this is in place due to the format of the corrected TIP4P gradient file
        
            #appends atom names and coordinates to appropriate lists
            grad_atom_list.append(data[1])
            grad_x_list.append(data[2])
            grad_y_list.append(data[3])
            grad_z_list.append(data[4])
            
            try:
                if int(data[0]) == int(atm_num): #checks if each atom read is last based on number of atoms specified in section 1
                    # runs functions to get distance and coulombic potential from gradient
                    atm_distance = distance(atoms_moved, grad_x_list, grad_y_list, grad_z_list)
                    distances.append(atm_distance)
                    
                    coulombic = charge(atm_num, charge_dict, grad_atom_list, grad_x_list, grad_y_list, grad_z_list)
                    coulomb_energy_list.append(coulombic)
                    
                    # Resets everything so that another gradient can be found
                    grad_section = 0 # resets section switch
                    grad_atom_list = [] 
                    grad_x_list = [] 
                    grad_y_list = [] 
                    grad_z_list = [] 
                    gradnum += 1
            except ValueError: # passes if the first value in a line cannot be made an integer
                pass
    except IndexError: # passes if there is not data in a line
        pass
    
gradient.close()

"""
5. Prints Data
"""
# First determines if data is in ascending order in terms of distance
# If not, re-orders lists so they go from shortest distance to longest
try:
    if distances[0] > distances[1]:
        distances.reverse()
        coulomb_energy_list.reverse()
        DFT_energy_list.reverse()
        
except IndexError: # passes if there are not at least 2 distances
    pass
    
print("Distance is in Angstroms, Coulombic Energy and Total DFT Energy are in kCal/mol.")
print("Distance       Coulombic        DFT")
for i in range(gradnum):
    print("%.9f   %.9f   %.9f" % (distances[i], coulomb_energy_list[i], DFT_energy_list[i]))
